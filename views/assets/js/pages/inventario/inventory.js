$('#sl-product').on('select2:select', function (e) { 
    $('input[name="code"]').val($(this).val());
    $('input[name="code"]').focus();
});

$('input[name="code"]').keydown(function(e) {
    var code = e.keyCode || e.which;
    if (code === 9) {  
        $('select[name="employed"]').select2('open');
    }
});

$('select[name="employed"]').on('select2:close', function (e) {
    $('input[name="date"]').focus();
});

$('input[name="reason"]').keydown(function(e) {
    var code = e.keyCode || e.which;
    if (code === 9) {  
        $('input[name="pending"]').prop('checked', true);
    }
});