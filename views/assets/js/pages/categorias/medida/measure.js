$('.update-measure').on('click', function () {
    var idMeasure =  $(this).parents("tr").find(".id-measure").html();
    var measure =  $(this).parents("tr").find(".measure").html();
    var unit =  $(this).parents("tr").find(".unit").html();

    $('input[name="id"]').val(idMeasure);
    $('input[name="measure-update"]').val(measure);
    $('input[name="unit-update"]').val(unit);
    
});