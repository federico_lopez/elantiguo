$('.update-employed').on('click', function () {

    var idEmployed =  $(this).parents("tr").find(".id-employed").html();
    var employed =  $(this).parents("tr").find(".name").html();
    var identification = $(this).parents("tr").find(".identification").html();
    var date =  $(this).parents("tr").find(".date").html();
    var city =  $(this).parents("tr").find(".city").html();
    var phone =  $(this).parents("tr").find(".phone").html();
    var position = $(this).parents("tr").find(".position").html();
    var salary =  $(this).parents("tr").find(".salary").html();

    $('input[name="id"]').val(idEmployed);
    $('input[name="name"]').val(employed);
    $('input[name="identification"]').val(identification);
    $('input[name="phone"]').val(phone);
    $('input[name="city"]').val(city);
    $('input[name="position"]').val(position);
    $('input[name="salary"]').val(salary);
    $('#btn-insert').val("ACTUALIZAR");

    var formattedDate = new Date(date);
    var day = ("0" + formattedDate.getDate()).slice(-2);
    var month = ("0" + (formattedDate.getMonth() + 1)).slice(-2);
    var dateNew = formattedDate.getFullYear()+"-"+(month)+"-"+(day) ;

    $('input[name="date"]').val(dateNew);
    console.log(formattedDate);

});

$('#btn-new').on('click', function () {
    $('input[name="id"]').val("0");
    $('#btn-insert').val("INSERTAR");
});