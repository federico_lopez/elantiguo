$('#sl-product').on('select2:close', function (e) {
    $('input[name="measure"]').focus();
});

$('select[name="unity"]').on('select2:close', function (e) {
    $('select[name="features"]').select2('open');
});

$('select[name="features"]').on('select2:close', function (e) {
    $('input[name="quantity"]').focus();
});

$('input[name=measure]').keydown(function(e) {
    var code = e.keyCode || e.which;
    if (code === 9) {  
        e.preventDefault();
        $('select[name="unity"]').select2('open');
    }
});