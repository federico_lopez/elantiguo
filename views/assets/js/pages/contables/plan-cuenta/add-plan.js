/*-------------------------------------Insertar Cliente Ajax------------------------------------------------------*/
$('form[name="insert-plan"]').submit(function(e){
    e.preventDefault();

    var data = $(this).serialize();
    var url = $(this).attr('action');
    var pathName = window.location.pathname;
    
    $.post(url, data, function(response, status, xhr){
        if(response>0){

            //if(pathName=="/plan-de-cuenta"){
            if(pathName=="/elantiguo/plan-de-cuenta"){
                alert("Insertado Correctamente");
                location.reload(true);
            }else{
                uploadSelect(response);
                $(this).trigger("reset");
            }
            
        }else{
            alert("El Plan de Cuenta ya existe o error de datos!!")
        }
     });
});

function uploadSelect(response){
    //carga el select insertado
    var plan = $('input[name="plan"]').val();
    $('select[name="plan"]').append($('<option>', {
        value: response,
        text: plan
    })).val(response).change();

    $('#modal-insert-plan').modal('toggle');

}
