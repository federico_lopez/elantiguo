$('.update-plan').on('click', function () {

    var idPlan =  $(this).parents("tr").find(".id-plan").html();
    var plan =  $(this).parents("tr").find(".plan").html();
    var code = $(this).parents("tr").find(".code").html();
    var level =  $(this).parents("tr").find(".level").html();

    $('input[name="id"]').val(idPlan);
    $('input[name="plan"]').val(plan);
    $('input[name="code"]').val(code);
    $('input[name="level"]').val(level);
    $('#btn-insert').val("ACTUALIZAR");

});

$('#btn-new').on('click', function () {
    $('form[name="insert-plan"]').trigger("reset");
    $('input[name="id"]').val("0");
    $('#btn-insert').val("INSERTAR");
});

$(document).on('keyup', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 113){
        $('#modal-insert-plan').modal('show');
        $('input[name="code"]').focus();
    }
  });

