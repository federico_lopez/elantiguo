$('.update-provider').on('click', function () {

    var idProvider =  $(this).parents("tr").find(".id-provider").html();
    var provider =  $(this).parents("tr").find(".provider").html();
    var ruc = $(this).parents("tr").find(".ruc").html();
    var contact =  $(this).parents("tr").find(".contact").html();
    var cellPhone =  $(this).parents("tr").find(".cellphone").html();
    var phone =  $(this).parents("tr").find(".phone").html();
    var location = $(this).parents("tr").find(".location").html();
    var address =  $(this).parents("tr").find(".address").html();
    var email =  $(this).parents("tr").find(".email").html();

    $('input[name="id"]').val(idProvider);
    $('input[name="provider"]').val(provider);
    $('input[name="ruc"]').val(ruc);
    $('input[name="contact"]').val(contact);
    $('input[name="cellphone"]').val(cellPhone);
    $('input[name="phone"]').val(phone);
    $('input[name="location"]').val(location);
    $('input[name="address"]').val(address);
    $('input[name="email"]').val(email);
    $('#btn-insert').val("ACTUALIZAR");

});

$('#btn-new').on('click', function () {
    $('input[name="id"]').val("0");
    $('#btn-insert').val("INSERTAR");
});