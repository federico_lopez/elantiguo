$('.update-customer').on('click', function () {

    var idCustomer =  $(this).parents("tr").find(".id-customer").html();
    var customer =  $(this).parents("tr").find(".customer").html();
    var ruc = $(this).parents("tr").find(".ruc").html();
    var contact =  $(this).parents("tr").find(".contact").html();
    var cellPhone =  $(this).parents("tr").find(".cellphone").html();
    var phone =  $(this).parents("tr").find(".phone").html();
    var location = $(this).parents("tr").find(".location").html();
    var address =  $(this).parents("tr").find(".address").html();
    var email =  $(this).parents("tr").find(".email").html();

    $('input[name="id"]').val(idCustomer);
    $('input[name="customer"]').val(customer);
    $('input[name="ruc"]').val(ruc);
    $('input[name="contact"]').val(contact);
    $('input[name="cellphone"]').val(cellPhone);
    $('input[name="phone"]').val(phone);
    $('input[name="location"]').val(location);
    $('input[name="address"]').val(address);
    $('input[name="email"]').val(email);
    $('#btn-insert').val("ACTUALIZAR");

});

$('#btn-new').on('click', function () {
    $('input[name="id"]').val("0");
    $('#btn-insert').val("INSERTAR");
});

$(document).on('keyup', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 113)
        $('#modal-insert-customer').modal('show');
  });