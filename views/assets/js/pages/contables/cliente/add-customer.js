/*-------------------------------------Insertar Cliente Ajax------------------------------------------------------*/
$('form[name="insert-customer"]').submit(function(e){
    e.preventDefault();

    var data = $(this).serialize();
    var url = $(this).attr('action');
    var pathName = window.location.pathname;
    
    $.post(url, data, function(response, status, xhr){
        if(response>0){

            $('#modal-insert-customer').modal('toggle');
            $(this).trigger("reset");

            //if(pathName=="/clientes"){
            if(pathName=="/elantiguo/clientes"){
                alert("Insertado Correctamente");
                location.reload(true);
            }else{
                uploadSelect(response);
            }
            
        }else{
            alert("El cliente ya existe o error de datos!!")
        }
     });
});

function uploadSelect(response){
    //carga el select insertado
    var customer = $('input[name="customer"]').val();
    $('#sl-customer').append($('<option>', {
        value: response,
        text: customer
    })).val(response).change();
}
