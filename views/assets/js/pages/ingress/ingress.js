$('input[name=remission]').keydown(function(e) {
    var code = e.keyCode || e.which;
    if (code === 9) {  
        e.preventDefault();
        $('select[name="customer"]').select2('open');
    }
});

$('select[name="customer"]').on('select2:close', function (e) {
    $('select[name="plan"]').select2('open');
});

$('select[name="plan"]').on('select2:close', function (e) {
    $('input[name="btn-insert"]').focus();
});