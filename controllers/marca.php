<?php

/*--------------------------------Index Marca---------------------------------------*/
$app->get('/marca', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';
        require_once 'models/Marca.php';

        $brand = new Marca();
        $brand->setEstado('1');
        $result = $brand->selectMarcaByEstado();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        //var_dump($customer);

        $app->render('categorias/marca/brand.html.twig', array(
            'user' => $userAr,
            'marca' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('brand');

/*--------------------------------INSERT Marca-------------------------------------------------*/
$app->post('/insert-marca', function() use($app){

    require_once 'models/Marca.php';
    $request = $app->request;

    $brand = new Marca();
    $brand->setMarca(strtoupper($request->post('brand')));
    $brand->setEstado('1');
    //echo $product->getProducto()."/".$product->getEstado();
    
    $insert=$brand->insertMarca();
    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Insertado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido insertar');
    }

    $app->redirect($app->urlFor('brand'));

})->name('insert-brand');

/*---------------------------------UPDATE Marca---------------------------------------*/
$app->post('/marca/modificar', function() use($app){

    require_once 'models/Marca.php';
    $request = $app->request;

    $brand = new Marca();
    $brand->setMarca(strtoupper($request->post('brand-update')));
    $brand->setIdMarca($request->post('id'));

    //echo $product->getProducto() ."/". $product->getIdProducto();

    $update=$brand->updateMarca();
    if($update){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Actualizado correctamente!!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido actualizar');
    }

    $app->redirect($app->urlFor('brand'));

})->name('update-brand');

/*---------------------------------UPDATE Estado Marca-------------------------------------------------------*/
$app->get('/modificar-estado-marca/:id', function($id) use($app){

    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Marca.php';

        $brand = new Marca();
        $brand->setIdMarca($id);
        $brand->setEstado("0");

        //echo $product->getEstado() ."/". $product->getIdProducto();

        $update=$brand->updateEstado();
        if($update){
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'Desactivado correctamente!!');
        }else{
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'No se ha podido actualizar');
        }

        $app->redirect($app->urlFor('brand'));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('update-state-brand');