<?php

/*--------------------------------------INSERT DETALLE FACTURA------------------------------------------*/
$app->post('/ingress-factura/insert-detalle-factura', function() use($app){

    require_once 'models/DetalleActivo.php';
    $request = $app->request;

    $detailActivo = new DetalleActivo();
    $detailActivo->setIdActivo($request->post('id-activo'));
    $detailActivo->setCantidad(intval(preg_replace('/[^0-9]+/','', $request->post('quantity'))));
    $detailActivo->setDetalle(strtoupper($request->post('detail')));
    $detailActivo->setCosto(intval(preg_replace('/[^0-9]+/','', $request->post('cost'))));
    $detailActivo->setTotales($detailActivo->getCantidad() * $detailActivo->getCosto());
    $impuesto = $request->post('tax');
    
    switch ($impuesto) {
        case 1:
            $detailActivo->setIvaDiez($detailActivo->getTotales()/11);
            $detailActivo->setGravadaDiez($detailActivo->getTotales()/1.1);
            break;
        case 2:
            $detailActivo->setIvaCinco($detailActivo->getTotales()/21);
            $detailActivo->setGravadaCinco($detailActivo->getTotales()/1.05);
            break;
        case 3:
            $detailActivo->setExenta($detailActivo->getTotales());
            break;
        case 4:
            $detailActivo->setTotales($detailActivo->getTotales()*-1);
            $detailActivo->setCosto($detailActivo->getTotales());
            break;
    }
    
    /*echo $detailActivo->getIdActivo()."/".$detailActivo->getCantidad()."/".$detailActivo->getCosto()."/".$detailActivo->getDetalle()."/".$detailActivo->getTotales();
    echo "\n".$detailActivo->getIvaDiez()."/".$detailActivo->getGravadaDiez()."/".$detailActivo->getIvaCinco()."/".$detailActivo->getGravadaCinco()."/".$detailActivo->getExenta();*/

    $detailActivo->insertDetalleActivo();
    $app->redirect($app->urlFor('ingress-update-facture',['id'=>$detailActivo->getIdActivo()]));

})->name('insert-detalle-activo');


/*--------------------------------------DELETE DETALLE ACTIVO--------------------------------------------------*/
$app->get('/detalle-activo/eliminar-detalle/:idactivo-:id', function($idactivo,$id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/DetalleActivo.php';

        $detailActivo = new DetalleActivo();
        $detailActivo->setIdDetalleActivo($id);
        $eliminado = $detailActivo->delDetalleActivo();

        if($eliminado){
            $app->flash('content', 'alert-success');
            $app->flash('mensaje', 'Producto Eliminado Correctamente');
        }

        $app->redirect($app->urlFor('ingress-update-facture',['id'=>$idactivo]));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('delete-detalle-activo');

