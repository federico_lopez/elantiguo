<?php

/*--------------------------------Index Medida---------------------------------------*/
$app->get('/medida', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';
        require_once 'models/Medida.php';

        $measure = new Medida();
        $measure->setEstado('1');
        $result = $measure->selectMedidaByEstado();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        //var_dump($customer);

        $app->render('categorias/medida/measure.html.twig', array(
            'user' => $userAr,
            'medida' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('measure');

/*--------------------------------INSERT Medida-------------------------------------------------*/
$app->post('/insert-medida', function() use($app){

    require_once 'models/Medida.php';
    $request = $app->request;

    $measure = new Medida();
    $measure->setMedida($request->post('measure'));
    $measure->setUnidad(strtoupper($request->post('unit')));
    $measure->setEstado('1');
    //echo $measure->getMedida()."/".$measure->getUnidad().$measure->getEstado();
    
    $insert=$measure->insertMedida();
    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Insertado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido insertar');
    }

    $app->redirect($app->urlFor('measure'));

})->name('insert-measure');

/*---------------------------------UPDATE Medida---------------------------------------*/
$app->post('/medida/modificar', function() use($app){

    require_once 'models/Medida.php';
    $request = $app->request;

    $measure = new Medida();
    $measure->setMedida(strtoupper($request->post('measure-update')));
    $measure->setUnidad(strtoupper($request->post('unit-update')));
    $measure->setIdMedida($request->post('id'));

    //echo $product->getProducto() ."/". $product->getIdProducto();

    $update=$measure->updateMedida();
    if($update){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Actualizado correctamente!!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido actualizar');
    }

    $app->redirect($app->urlFor('measure'));

})->name('update-measure');

/*---------------------------------UPDATE Estado Medida-------------------------------------------------------*/
$app->get('/modificar-estado-medida/:id', function($id) use($app){

    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Medida.php';

        $measure = new Medida();
        $measure->setIdMedida($id);
        $measure->setEstado("0");

        //echo $product->getEstado() ."/". $product->getIdProducto();

        $update=$measure->updateEstado();
        if($update){
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'Desactivado correctamente!!');
        }else{
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'No se ha podido actualizar');
        }

        $app->redirect($app->urlFor('measure'));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('update-state-measure');