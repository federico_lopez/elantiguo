<?php

/*--------------------------------Index Plan de Cuenta------------------------------------------------------------*/
$app->get('/plan-de-cuenta', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';
        require_once 'models/PlanCuenta.php';

        $plan = new PlanCuenta();
        $result = $plan->selectAll();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        //var_dump($result);

        $app->render('contables/plan-cuenta/plan.html.twig', array(
            'user' => $userAr,
            'plan' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('plan');

/*-------------------------------------Buscar Plan----------------------------------------------------------------*/
$app->post('/busqueda-plan', function() use($app){

    require_once 'models/Selectores.php';
    require_once 'models/PlanCuenta.php';
    $request = $app->request;

    $selector = new Selectores();
    $userAr = $selector->returnRol();

    $plan = new PlanCuenta();
    $plan->setPlan(strtolower($request->post('buscar')));
    $result = $plan->searchPlan();

    $app->render('contables/plan-cuenta/plan.html.twig', array(
        'user' => $userAr,
        'plan' => $result
    ));

})->name('search-plan');

/*---------------------------------UPDATE Estado Plan de Cuenta---------------------------------------------------*/
$app->get('/modificar-estado-plan/:id', function($id) use($app){

    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/PlanCuenta.php';

        $plan = new PlanCuenta();
        $plan->setIdPlanCuenta($id);
        $plan->setEstado("0");

        $update=$plan->updateEstado();
        if($update){
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'Desactivado correctamente!!');
        }else{
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'No se ha podido actualizar');
        }

        $app->redirect($app->urlFor('plan'));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('update-state-plan');

/*--------------------------------INSERT Plan de Cuenta-----------------------------------------------------------*/
$app->post('/insert-plan', function() use($app){

    require_once 'models/PlanCuenta.php';
    $request = $app->request;

    $plan = new PlanCuenta();
    $plan->setPlan(strtoupper($request->post('plan')));
    $plan->setCodigo($request->post('code'));
    $plan->setNivel($request->post('level'));
    $plan->setEstado('1');
    $id=$request->post('id');
    
    $insert;
    if($id>0){
        $plan->setIdPlanCuenta($id);
        $insert = $plan->updatePlan();
    }else{
        $insert=$plan->insertPlanCuenta();
    }

    print $insert;

})->name('insert-plan');


