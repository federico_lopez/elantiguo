<?php

/*---------------------------------------Pagina cargar Inventario------------------------------------------------*/
$app->get('/inventario/salida', function() use($app){

    //redireccionar si hay session
    if(!empty($_SESSION['session'])){
        require_once 'models/Inventario.php';
        require_once 'models/Selectores.php';

        $inventory = new Inventario();
        $inventory->setMovimiento("0");
        $result = $inventory->selectByMovimiento();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        $employed = $selector->cargarEmpleados();
        $product = $selector->cargarFetchAll('SELECT concat(producto,": ",cantidad) producto, codigo FROM stock ORDER BY producto;');

        $app->render('inventario/inventory.html.twig', array(
            'user' => $userAr,
            'empleado' => $employed,
            'producto' => $product,
            'inventario' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('inventory');

/*---------------------------------------Pagina Entrada Inventario------------------------------------------------*/
$app->get('/inventario/entrada', function() use($app){

    //redireccionar si hay session
    if(!empty($_SESSION['session'])){
        require_once 'models/Inventario.php';
        require_once 'models/Selectores.php';

        $inventory = new Inventario();
        $inventory->setMovimiento("1");
        $result = $inventory->selectByMovimiento();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        $employed = $selector->cargarEmpleados();
        $product = $selector->cargarFetchAll('SELECT concat(producto,": ",cantidad) producto, codigo FROM stock ORDER BY producto;');

        $app->render('inventario/entry.html.twig', array(
            'user' => $userAr,
            'empleado' => $employed,
            'producto' => $product,
            'inventario' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('entry');

/*---------------------------------INSERT Inventario-------------------------------------------------------------*/
$app->post('/inventario/insertar', function() use($app){

    require_once 'models/Inventario.php';
    require_once 'models/Stock.php';

    $request = $app->request;
    $stock = new Stock();
    $inventory = new Inventario();

    $inventory->setMovimiento($request->post('movement'));
    $inventory->setIdEmpleado($request->post('employed'));
    $inventory->setCantidad(intval(preg_replace('/[^0-9]+/','', $request->post('quantity'))));
    $inventory->setMotivo(strtoupper($request->post('reason')));

    $fecha=date_create($request->post('date'));
    $dateInventory = date_format($fecha,"Y/m/d");
    $inventory->setFecha($dateInventory);

    $inventory->setPendiente(0);
    $checked = $request->post('pending');
    if($checked){
        $inventory->setPendiente(1);
    }

    $stock->setCodigo($request->post('code'));
    $resultStock = $stock->selectStockByCode();
    
    /*----Si el codigo existe inserta en la tabla inventario----*/
    if($resultStock){

        $cantidadStock = $resultStock["cantidad"];
        $idStock = $resultStock["id_stock"];
        $cantidadInventario = $inventory->getCantidad();

        if($inventory->getMovimiento() == "0"){
            $total = $cantidadStock - $cantidadInventario;
            if($total < 0){
                $app->flash('content', 'alert-danger');
                $app->flash('mensaje', 'La cantidad es mayor al existente');
                $app->redirect($app->urlFor('inventory'));
            }
        }else{
            $total = $cantidadStock + $cantidadInventario;
        }

        //modificar la cantidad del stock
        $stock->setCantidad($total);
        $stock->setIdStock($idStock);
        $stock->updateQuantity();
        
        //insertar el movimiento en el inventario
        $inventory->setIdStock($idStock);
        $inventory->insertInventario();
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Cargado correctamente!!');

        if($inventory->getMovimiento() == "0")
            $app->redirect($app->urlFor('inventory'));
        else
            $app->redirect($app->urlFor('entry'));


    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'El codigo no existe');
        $app->redirect($app->urlFor('inventory'));
    }
    
})->name('insert-inventory');

