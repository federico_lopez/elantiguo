<?php

/*--------------------------------Index Caracteristica---------------------------------------*/
$app->get('/caracteristica', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';
        require_once 'models/Caracteristica.php';

        $feature = new Caracteristica();
        $feature->setEstado('1');
        $result = $feature->selectCaracteristicaByEstado();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        //var_dump($customer);

        $app->render('categorias/caracteristica/feature.html.twig', array(
            'user' => $userAr,
            'caracteristica' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('feature');

/*--------------------------------INSERT Caracteristica----------------------------------------------------------*/
$app->post('/insert-caracteristica', function() use($app){

    require_once 'models/Caracteristica.php';
    $request = $app->request;

    $feature = new Caracteristica();
    $feature->setCaracteristica(strtoupper($request->post('features')));
    $feature->setEstado('1');
    //echo $product->getProducto()."/".$product->getEstado();
    
    $insert=$feature->insertCaracteristica();
    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Insertado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido insertar');
    }

    $app->redirect($app->urlFor('feature'));

})->name('insert-feature');

/*---------------------------------UPDATE caracteristica---------------------------------------*/
$app->post('/caracteristica/modificar', function() use($app){

    require_once 'models/Caracteristica.php';
    $request = $app->request;

    $feature = new Caracteristica();
    $feature->setCaracteristica(strtoupper($request->post('features-update')));
    $feature->setIdCaracteristica($request->post('id'));

    //echo $product->getProducto() ."/". $product->getIdProducto();

    $update=$feature->updateCaracteristica();
    if($update){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Actualizado correctamente!!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido actualizar');
    }

    $app->redirect($app->urlFor('feature'));

})->name('update-feature');

/*---------------------------------UPDATE Estado Caracteristica--------------------------------------------------*/
$app->get('/modificar-estado-caracteristica/:id', function($id) use($app){

    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Caracteristica.php';

        $feature = new Caracteristica();
        $feature->setIdCaracteristica($id);
        $feature->setEstado("0");

        //echo $product->getEstado() ."/". $product->getIdProducto();

        $update=$feature->updateEstado();
        if($update){
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'Desactivado correctamente!!');
        }else{
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'No se ha podido actualizar');
        }

        $app->redirect($app->urlFor('feature'));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('update-state-feature');