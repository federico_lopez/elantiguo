<?php

/*--------------------------------Index Proveedores---------------------------------------------------------------*/
$app->get('/proveedores', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';
        require_once 'models/Proveedor.php';

        $provider = new Proveedor();
        $result = $provider->selectAll();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        //var_dump($result);

        $app->render('categorias/proveedor/provider.html.twig', array(
            'user' => $userAr,
            'proveedor' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('provider');

/*-------------------------------------Buscar Proveedor-----------------------------------------------------------*/
$app->post('/busqueda-proveedor', function() use($app){

    require_once 'models/Selectores.php';
    require_once 'models/Proveedor.php';
    $request = $app->request;

    $selector = new Selectores();
    $userAr = $selector->returnRol();

    $provider = new Proveedor();
    $provider->setProveedor(strtolower($request->post('buscar')));
    $result = $provider->searchProvider();

    $app->render('categorias/proveedor/provider.html.twig', array(
        'user' => $userAr,
        'proveedor' => $result
    ));

})->name('search-provider');

/*---------------------------------UPDATE Estado Proveedor--------------------------------------------------------*/
$app->get('/modificar-estado-proveedor/:id', function($id) use($app){

    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Proveedor.php';

        $provider = new Proveedor();
        $provider->setIdProveedor($id);
        $provider->setEstado("0");

        $update=$provider->updateEstado();
        if($update){
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'Desactivado correctamente!!');
        }else{
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'No se ha podido actualizar');
        }

        $app->redirect($app->urlFor('provider'));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('update-state-provider');

/*--------------------------------INSERT Cliente------------------------------------------------------------------*/
$app->post('/insert-provider', function() use($app){

    require_once 'models/Proveedor.php';
    $request = $app->request;

    $provider = new Proveedor();
    $provider->setProveedor(strtoupper($request->post('provider')));
    $provider->setRuc($request->post('ruc'));
    $provider->setContacto(strtoupper($request->post('contact')));
    $provider->setCelular($request->post('cellphone'));
    $provider->setTelefono($request->post('phone'));
    $provider->setCiudad($request->post('location'));
    $provider->setDireccion($request->post('address'));
    $provider->setEmail($request->post('email'));

    $id=$request->post('id');
    $insert;
    if($id>0){
        $provider->setIdProveedor($id);
        $insert = $provider->updateProvider();
    }else{
        $insert=$provider->insertProvider();
    }

    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Registrado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido registrar');
    }

    $app->redirect($app->urlFor('provider'));

})->name('insert-provider');