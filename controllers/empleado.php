<?php

/*--------------------------------Index Empleados----------------------------------------------------------------*/
$app->get('/empleados', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';
        require_once 'models/Empleado.php';

        $employed = new Empleado();
        $result = $employed->selectEmpleado();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        //var_dump($customer);

        $app->render('categorias/empleados/employed.html.twig', array(
            'user' => $userAr,
            'empleado' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('employed');

/*--------------------------------INSERT Empleado----------------------------------------------------------------*/
$app->post('/insert-employed', function() use($app){

    require_once 'models/Empleado.php';
    $request = $app->request;

    $employed = new Empleado();
    $employed->setNombre(strtoupper($request->post('name')));
    $employed->setCi(intval(preg_replace('/[^0-9]+/','', $request->post('identification'))));
    $employed->setTelefono($request->post('phone'));
    $employed->setCiudad(strtoupper($request->post('city')));
    $employed->setCargo(strtoupper($request->post('position')));
    $employed->setSueldo(intval(preg_replace('/[^0-9]+/','', $request->post('salary'))));

    $fecha=date_create($request->post('date'));
    $date = date_format($fecha,"Y/m/d");
    $employed->setNacimiento($date);
    //echo $product->getProducto()."/".$product->getEstado();

    $id=$request->post('id');
    $insert;
    if($id>0){
        $employed->setIdEmpleado($id);
        $insert = $employed->updateEmpleado();
    }else{
        $insert=$employed->insertEmpleado();
    }

    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Registrado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido registrar');
    }

    $app->redirect($app->urlFor('employed'));

})->name('insert-employed');


/*---------------------------------UPDATE Estado Empleado--------------------------------------------------------*/
$app->get('/modificar-estado-empleado/:id', function($id) use($app){

    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Empleado.php';

        $employed = new Empleado();
        $employed->setIdEmpleado($id);
        $employed->setEstado("0");

        //echo $product->getEstado() ."/". $product->getIdProducto();

        $update=$employed->updateEstado();
        if($update){
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'Desactivado correctamente!!');
        }else{
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'No se ha podido actualizar');
        }

        $app->redirect($app->urlFor('employed'));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('update-state-employed');