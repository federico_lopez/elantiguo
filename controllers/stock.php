<?php

/*----------------------------------------Pagina cargar stock----------------------------------------------------*/
$app->get('/stock', function() use($app){

    //redireccionar si hay session
    if(!empty($_SESSION['session'])){
        require_once 'models/Stock.php';
        require_once 'models/Selectores.php';

        $stock = new Stock();
        $result = $stock->selectStock();

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        $producto = $selector->uploadProducto();
        $caracteristica = $selector->uploadCaracteristica();
        $medida = $selector->uploadMedida();


        $app->render('categorias/stock/stock.html.twig', array(
            'user' => $userAr,
            'producto' => $producto,
            'caracteristica' => $caracteristica,
            'medida' => $medida,
            'stock' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('stock');



/*---------------------------------INSERT stock------------------------------------------------------------------*/
$app->post('/stock/insertar', function() use($app){

    require_once 'models/Stock.php';
    require_once 'models/DetalleStock.php';
    require_once 'models/Caracteristica.php';
    require_once 'models/Producto.php';

    $request = $app->request;

    $stock = new Stock();
    $detail = new DetalleStock();
    $feature = new Caracteristica();
    $product = new Producto();

    $detail->setIdDeposito(strtoupper($request->post('deposit')));
    $detail->setIdProducto($request->post('product'));
    $detail->setDetalleStock(strtoupper($request->post('measure')));
    $detail->setIdMedida($request->post('unity'));
    $detail->setIdCaracteristica($request->post('features'));
    
    $feature->setIdCaracteristica($detail->getIdCaracteristica());
    $stockFeature = $feature->selectCaracteristicaByID();

    $product->setIdProducto($detail->getIdProducto());
    $stockProduct = $product->selectProductByID();

    $productStock = $stockProduct['producto']." ".$detail->getDetalleStock()." ".$stockFeature['caracteristica'];

    $insertDetailStock=$detail->insertDetalleStock();
    if(empty($insertDetailStock)){
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'Producto Repetido o erroneo');
        $app->redirect($app->urlFor('product'));
    }
    
    $codeStock =  $detail->getIdProducto().$insertDetailStock.$detail->getIdCaracteristica().$detail->getIdDeposito();
    $stock->setCantidad($request->post('quantity'));
    $stock->setStockMin($request->post('minimum'));
    $stock->setUbicacion(strtoupper($request->post('location')));
    $stock->setCosto(intval(preg_replace('/[^0-9]+/','', $request->post('cost'))));
    $stock->setCodigo($codeStock);
    $stock->setProducto($productStock);
    $stock->setIdDetalleStock($insertDetailStock);

    $insertStock =  $stock->insertStock();

    if($insertStock){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Cargado correctamente!!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido cargar');
    }
   
    /*echo $detail->getIdDeposito() ."/".$detail->getIdProducto() ."/".$detail->getIdCaracteristica()."/";
    echo $detail->getDetalleStock() ."/".$detail->getIdMedida() ."/".$productStock."/";
    echo $stock->getCantidad() ."/".$stock->getStockMin()."/".$stock->getUbicacion()."/".$stock->getCosto();*/

    $app->redirect($app->urlFor('stock'));

})->name('insert-stock');