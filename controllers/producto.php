<?php

/*--------------------------------Index Producto---------------------------------------*/
$app->get('/productos', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';
        require_once 'models/Producto.php';

        $product = new Producto();
        $product->setEstado('1');
        $result = $product->selectProductoByEstado();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        //var_dump($customer);

        $app->render('categorias/productos/product.html.twig', array(
            'user' => $userAr,
            'producto' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('product');

/*--------------------------------INSERT Producto-------------------------------------------------*/
$app->post('/insert-producto', function() use($app){

    require_once 'models/Producto.php';
    $request = $app->request;

    $product = new Producto();
    $product->setProducto(strtoupper($request->post('product')));
    $product->setEstado('1');
    //echo $product->getProducto()."/".$product->getEstado();
    
    $insert=$product->insertProducto();
    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Insertado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido insertar');
    }

    $app->redirect($app->urlFor('product'));

})->name('insert-product');

/*---------------------------------UPDATE Producto---------------------------------------*/
$app->post('/producto/modificar', function() use($app){

    require_once 'models/Producto.php';
    $request = $app->request;

    $product = new Producto();
    $product->setProducto(strtoupper($request->post('product-update')));
    $product->setIdProducto($request->post('id'));

    //echo $product->getProducto() ."/". $product->getIdProducto();

    $update=$product->updateProducto();
    if($update){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Actualizado correctamente!!');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido actualizar');
    }

    $app->redirect($app->urlFor('product'));

})->name('update-product');

/*---------------------------------UPDATE Estado Producto---------------------------------------*/
$app->get('/modificar-estado/:id', function($id) use($app){

    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Producto.php';

        $product = new Producto();
        $product->setIdProducto($id);
        $product->setEstado("0");

        //echo $product->getEstado() ."/". $product->getIdProducto();

        $update=$product->updateEstado();
        if($update){
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'Desactivado correctamente!!');
        }else{
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'No se ha podido actualizar');
        }

        $app->redirect($app->urlFor('product'));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('update-state-product');