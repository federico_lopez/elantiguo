<?php

/*--------------------------------Index Clientes------------------------------------------------------------------*/
$app->get('/clientes', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';
        require_once 'models/Cliente.php';

        $customer = new Cliente();
        $result = $customer->selectAll();
        //var_dump($result);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        //var_dump($result);

        $app->render('contables/cliente/customer.html.twig', array(
            'user' => $userAr,
            'cliente' => $result
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('customer');

/*-------------------------------------Buscar Cliente-------------------------------------------------------------*/
$app->post('/busqueda-cliente', function() use($app){

    require_once 'models/Selectores.php';
    require_once 'models/Cliente.php';
    $request = $app->request;

    $selector = new Selectores();
    $userAr = $selector->returnRol();

    $customer = new Cliente();
    $customer->setCliente(strtolower($request->post('buscar')));
    $result = $customer->searchCustomer();

    $app->render('contables/cliente/customer.html.twig', array(
        'user' => $userAr,
        'cliente' => $result
    ));

})->name('search-customer');

/*---------------------------------UPDATE Estado Cliente----------------------------------------------------------*/
$app->get('/modificar-estado-cliente/:id', function($id) use($app){

    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Cliente.php';

        $customer = new Cliente();
        $customer->setIdCliente($id);
        $customer->setEstado("0");

        $update=$customer->updateEstado();
        if($update){
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'Desactivado correctamente!!');
        }else{
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'No se ha podido actualizar');
        }

        $app->redirect($app->urlFor('customer'));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('update-state-customer');

/*--------------------------------INSERT Cliente------------------------------------------------------------------*/
$app->post('/insert-customer', function() use($app){

    require_once 'models/Cliente.php';
    $request = $app->request;

    $customer = new Cliente();
    $customer->setCliente(strtoupper($request->post('customer')));
    $customer->setRuc($request->post('ruc'));
    $customer->setContacto(strtoupper($request->post('contact')));
    $customer->setCelular($request->post('cellphone'));
    $customer->setTelefono($request->post('phone'));
    $customer->setLocalidad($request->post('location'));
    $customer->setDireccion($request->post('address'));
    $customer->setEmail($request->post('email'));

    $id=$request->post('id');
    
    $insert;
    if($id>0){
        $customer->setIdCliente($id);
        $insert = $customer->updateCliente();
    }else{
        $insert=$customer->insertCliente();
    }

    print $insert;

    /*if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Registrado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido registrar');
    }

    $app->redirect($app->urlFor('customer'));*/

})->name('insert-customer');


