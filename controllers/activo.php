<?php

/*-------------------------------------------Index Ingresos----------------------------------------------------*/
$app->get('/ingresos', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Activo.php';
        require_once 'models/Selectores.php';

        $mes=date("m");
        $anho=date("Y");
        $ingress = new Activo();
        //$ingress->setEstado("1");
        $ingresMonth = $ingress->ingressMonth($mes, $anho);

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        $mesLetra = $selector->returnMes($mes);

        $fechas = array(
            "mes"  => $mesLetra,
            "anho" => $anho,
        );

        $app->render('activo/ingress.html.twig', array(
            'ingress' => $ingresMonth,  'fechas' => $fechas, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('ingress');

/*-----------------------------------Pagina Insertar Facturas-------------------------------------------------*/
$app->get('/ingresos-vemtas', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        $customer = $selector->cargarClientes();
        $stamp = $selector->uploadTimbrado();
        $plan = $selector->uploadPlan();

        $app->render('activo/ingress-insert-facture.html.twig', array(
            'cliente' => $customer, 'timbrado' => $stamp, 'plan'=>$plan, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('ingress-facture');

/*-----------------------------------Pagina Update Facturas----------------------------------------------------*/
$app->get('/factura-ventas/:id', function($id) use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Activo.php';
        require_once 'models/DetalleActivo.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        $customer = $selector->cargarClientes();
        $stamp = $selector->uploadTimbrado();
        $plan = $selector->uploadPlan();

        $active = new Activo();
        $active->setIdActivo($id);
        $resultActive = $active->selectActivoById();
        //var_dump($resultActive);

        $detailActive = new DetalleActivo();
        $detailActive->setIdActivo($id);
        $resultDetalleActive = $detailActive->selectDetalleFactura();
        $resultSumDetail = $detailActive->selectSumDetalleFactura();
        //var_dump($resultSumDetail);

        $app->render('activo/ingress-update-facture.html.twig', array(
            'cliente' => $customer, 'timbrado' => $stamp, 'user' => $userAr, 'detalle' => $resultDetalleActive,
            'activo' => $resultActive,  'plan'=>$plan, 'total' => $resultSumDetail
        ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('ingress-update-facture');

/*-------------------------------------INSERT ACTIVO-----------------------------------------------------------*/
$app->post('/ingress/insert-activo', function() use($app){

    require_once 'models/Activo.php';
    $ingress = new Activo();

    $request = $app->request;

    $fecha=date_create($request->post('date'));
    $date = date_format($fecha,"Y/m/d");
    $ingress->setFecha($date);

    $ingress->setCondicion($request->post('condition'));
    $ingress->setIdCliente($request->post('customer'));
    $ingress->setNroFactura($request->post('fac-number'));
    $ingress->setEstado('1');
    $ingress->setIdPlanCuenta($request->post('plan'));
    $ingress->setIdTimbrado($request->post('stamp'));
    $ingress->setIdDocumento($request->post('document'));
    $ingress->setRemision($request->post('remission'));

    echo $ingress->getFecha()."/".$ingress->getNroFactura()."/".$ingress->getEstado()."/".$ingress->getCondicion()."/".$ingress->getIdCliente()."/".$ingress->getIdPlanCuenta()."/".
    $ingress->getIdTimbrado()."/".$ingress->getIdDocumento()."/".$ingress->getRemision();

    $lastId=$ingress->insertActivo();

    if($lastId){
        $app->redirect($app->urlFor('ingress-update-facture',['id'=>$lastId]));
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'Datos incorrectos');
        $app->redirect($app->urlFor('ingress-facture'));
    }

})->name('insert-activo');

/*----------------------------------------------UPDATE ACTIVO--------------------------------------------------*/
$app->post('/ingresos-factura/update', function() use($app){

    require_once 'models/Activo.php';
    $ingress = new Activo();
    $request = $app->request;

    $fecha=date_create($request->post('date'));
    $date = date_format($fecha,"Y/m/d");
    $ingress->setFecha($date);

    $ingress->setIdActivo($request->post('id-activo'));
    $ingress->setIdCliente($request->post('customer'));
    $ingress->setNroFactura($request->post('fac-number'));
    $ingress->setEstado($request->post('state'));
    $ingress->setTipoFactura($request->post('select-ingress'));
    $ingress->setObservacion($request->post('observations'));

    if($ingress->getTipoFactura() > 0)
        $ingress->setTipoIngress("D");
    else        
        $ingress->setTipoIngress("M");

    //echo $ingress->getFecha()."/".$ingress->getIdActivo()."/".$ingress->getNroFactura()."/".$ingress->getIdCliente()."/".$ingress->getEstado();
    
    $actualizado = $ingress->updateActivo();
    if($actualizado){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Editado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se ha podido modificar');
    }

    $app->redirect($app->urlFor('ingress-update-facture',['id'=>$ingress->getIdActivo()]));

})->name('update-activo');



/*-------------------------------------------ANULAR INGRESOS---------------------------------------------------*/
$app->get('/ingresos/anular/:id-:pagina', function($id,$redirect) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Activo.php';

        $ingress = new Activo();
        $ingress->setIdActivo($id);
        $ingress->setTipoIngress("A");
        $ingress->setTipoFactura("5");
        $anulado = $ingress->anularActivo();

        if($anulado){
            $app->flash('content', 'alert-danger');
            $app->flash('mensaje', 'ANULADO CORRECTAMENTE');
        }

        $app->redirect($app->urlFor($redirect));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('null-activo');


/*---------------------------------------------INDEX REPORTE INGRESO--------------------------------------------------*/
$app->get('/reporte-ingreso', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Activo.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();
        //$client = $selector->cargarClientes();
        //$products = $selector->uploadProducto();

        $app->render('ingress/report-ingress.html.twig', array(
            'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('report-ingress');

/*-----------------------------------------------BUSQUEDA CLIENTE INGRESO---------------------------------------------*/
$app->post('/busqueda-ingreso-cliente', function() use($app){

    require_once 'models/Activo.php';
    require_once 'models/Selectores.php';

    $selector = new Selectores();
    $ingress = new Activo();
    $request = $app->request;

    $ingress->setEstado($request->post('customer'));
    $result = $ingress->searchCustomer();
    $userAr = $selector->returnRol();

    $app->render('ingress/report-ingress.html.twig', array(
        'ingress' => $result, 'user' => $userAr));

})->name('search-customer-ingress');

/*-------------------------------------BUSQUEDA CLIENTE INGRESO---------------------------------------------*/
$app->post('/busqueda-ingreso-producto', function() use($app){

    require_once 'models/Activo.php';
    require_once 'models/Selectores.php';

    $selector = new Selectores();
    $ingress = new Activo();
    $request = $app->request;

    $ingress->setEstado($request->post('product'));
    $result = $ingress->searchProduct();
    $userAr = $selector->returnRol();

    $app->render('ingress/report-ingress.html.twig', array(
        'ingress' => $result, 'user' => $userAr));

})->name('search-product-ingress');



/*----------Busqueda Mensual Ingresos----------*/
$app->post('/busqueda-ingreso', function() use($app){

    require_once 'models/Activo.php';
    require_once 'models/Selectores.php';

    $selector = new Selectores();
    $request = $app->request;
    $mes=$request->post('mes');
    $anho=$request->post('anho');

    $ingress = new Activo();
    $ingress->setEstado("F");
    $ingresMonth = $ingress->ingressMonth($mes, $anho);

    $ingress->setTipoIngress("M");
    $totalMoney = $ingress->sumporTipoIngres($mes,$anho);
    $totalM = $totalMoney["total"];

    $ingress->setTipoIngress("D");
    $totalDocument = $ingress->sumporTipoIngres($mes,$anho);
    $totalD = $totalDocument["total"];

    $ingress->setTipoIngress("A");
    $totalNull = $ingress->sumporTipoIngres($mes,$anho);
    $totalA = $totalNull["total"];

    $sumTotal = $totalM+$totalD;

    $total = array(
        "money"  => $totalM,
        "document" => $totalD,
        "null" => $totalA,
        "total" => $sumTotal
    );

    $fechas = array(
        "mes"  => $mes,
        "anho" => $anho,
    );

    $userAr = $selector->returnRol();

    $app->render('ingress/ingress.html.twig', array(
        'ingress' => $ingresMonth, 'total' => $total, 'fechas' => $fechas, 'user' => $userAr));

})->name('busqueda-ingress');