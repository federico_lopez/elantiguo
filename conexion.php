<?php

class Conexion extends PDO
{
    private $bd = 'mysql';
    private $host = 'localhost';
    private $name = 'alantico_elantiguo2020';
    private $user = 'root';
    private $pass = 'password';
    /*private $user = 'alantico_admin';
    private $pass = 'admin123';*/
    public function __construct()
    {
        //Sobreescribo el método constructor de la clase PDO.
        try {
            parent::__construct($this->bd . ':host=' . $this->host . ';dbname=' . $this->name, $this->user, $this->pass);
        } catch (PDOException $e) {
            echo 'PDO exception: ' . $e->getMessage();
            exit;
        }
    }
}
