<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 03/05/2016
 * Time: 01:12 PM
 */
class Selectores
{

    public function cargarEmpleados(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_empleado, nombre FROM empleado order by nombre asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function cargarStock(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idstock, CONCAT(producto, ' - E:', cantidad) producto, producto 'stock', costo FROM stock order by producto asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function cargarClientes(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_cliente, cliente FROM cliente order by cliente asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function uploadProvider(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idproveedor,proveedor FROM proveedor order by proveedor asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function uploadCategory(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idcategoria,categoria FROM categoria order by categoria asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function uploadProducto(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_producto,producto FROM producto order by producto asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function uploadCaracteristica(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_caracteristica,caracteristica FROM caracteristica order by caracteristica asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function uploadMedida(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_medida, CONCAT(upper(medida),' ' , lower(unidad)) medida  FROM medida order by medida asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function uploadPlan(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_plan_cuenta, CONCAT(codigo, ' ', plan) plan  FROM plan_cuenta order by codigo asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function uploadTimbrado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT * FROM timbrado WHERE estado='1';");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }


    public function returnRol(){
        $userAr['iduser'] = $_SESSION['session']['id_user'];
        $userAr['idrol'] = $_SESSION['session']['rol'];
        $userAr['imagen'] = $_SESSION['session']['imagen'];
        $userAr['user'] = $_SESSION['session']['user'];
        return $userAr;
    }


    public function cargarFetchAll($sentencia){
        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function cargarFetch($sentencia){
        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }
   
    /** Actual month last day **/
    public function lastDay() { 
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
    }
 
    /** Actual month first day **/
    function firstDay() {
        $month = date('m');
        $year = date('Y');
        return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
    }

    //retorna el mes en letra
    function returnMes($mes) {
        
        switch ($mes){
            case 1: return "Enero"; break;
            case 2: return "Febrero"; break;
            case 3: return "Marzo"; break;
            case 4: return "Abril"; break;
            case 5: return "Mayo"; break;
            case 6: return "Junio"; break;
            case 7: return "Julio"; break;
            case 8: return "Agosto"; break;
            case 9: return "Septiembre"; break;
            case 10: return "Octubre"; break;
            case 11: return "Noviembre"; break;
            case 12: return "Diciembre"; break;
        }
         
        //return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
    }



}