<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 01/01/2020
 * Time: 01:12 PM
 */
class Medida{

    private $idMedida;
    private $medida;
    private $unidad;
    private $estado;
        
   /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of unidad
     */ 
    public function getUnidad()
    {
        return $this->unidad;
    }

    /**
     * Set the value of unidad
     *
     * @return  self
     */ 
    public function setUnidad($unidad)
    {
        $this->unidad = $unidad;

        return $this;
    }

    /**
     * Get the value of medida
     */ 
    public function getMedida()
    {
        return $this->medida;
    }

    /**
     * Set the value of medida
     *
     * @return  self
     */ 
    public function setMedida($medida)
    {
        $this->medida = $medida;

        return $this;
    }

    /**
     * Get the value of idMedida
     */ 
    public function getIdMedida()
    {
        return $this->idMedida;
    }

    /**
     * Set the value of idMedida
     *
     * @return  self
     */ 
    public function setIdMedida($idMedida)
    {
        $this->idMedida = $idMedida;

        return $this;
    }

     //--------------------------------------------------------End Getter and Setter---------------------------------------------------------------------------

    /**
     * insert the data in the table medida
     */ 
    public function insertMedida(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO medida(medida,unidad,estado)
                                    VALUES (:medida,:unidad,:estado);");
        $query->execute(array(
            'medida' => $this->getMedida(),
            'unidad' => $this->getUnidad(),
            'estado' => $this->getEstado()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * get the result of the medida table by estado
     */ 
    public function selectMedidaByEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_medida, medida, unidad, estado
                                    FROM medida
                                    WHERE estado = :estado
                                    ORDER BY id_medida DESC;");
        $query->execute(array('estado' => $this->getEstado()));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    
    /**
     * update the data in the table medida
     */ 
    public function updateMedida(){
        $conexion = new Conexion();
        $query = $conexion->prepare(
            "UPDATE medida
            SET  medida = :medida, unidad = :unidad
            WHERE id_medida =:id;"
        );
        $query->execute(array(
            'medida' => $this->getMedida(),
            'unidad' => $this->getUnidad(),
            'id' => $this->getIdMedida()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * update the estado in the table medida
     */ 
    public function updateEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE medida
                                    SET  estado = :estado
                                     WHERE id_medida =:id;");
        $query->execute(array(
            'estado' => $this->getEstado(),
            'id' => $this->getIdMedida()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * get the result of the medida table by id
     */ 
    public function selectMedidaByID(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_medida, medida
                                    FROM medida
                                    WHERE id_medida = :id;");
        $query->execute(array('id' => $this->getIdMedida()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }
    

   

   
}