<?php

/**
 * Created by PhpStorm.
 * User: Fede
 * Date: 3/5/2017
 * Time: 10:56 AM
 */
class PlanCuenta{
    private $idPlanCuenta;
    private $codigo;
    private $nivel;
    private $plan;
    private $estado;

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of plan
     */ 
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set the value of plan
     *
     * @return  self
     */ 
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get the value of nivel
     */ 
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set the value of nivel
     *
     * @return  self
     */ 
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get the value of codigo
     */ 
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set the value of codigo
     *
     * @return  self
     */ 
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get the value of idPlanCuenta
     */ 
    public function getIdPlanCuenta()
    {
        return $this->idPlanCuenta;
    }

    /**
     * Set the value of idPlanCuenta
     * @return  self
     */ 
    public function setIdPlanCuenta($idPlanCuenta)
    {
        $this->idPlanCuenta = $idPlanCuenta;

        return $this;
    }

    //--------------------------------------End Getter and Setter---------------------------------------------------

    public function selectAll(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_plan_cuenta,codigo,nivel,plan,estado 
                                    FROM plan_cuenta
                                    WHERE estado='1'
                                    order by codigo asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    /**
     * search data in table plan_cuente where plan like 
     */ 
    public function searchPlan(){
        $conexion = new Conexion();
        $likeVar = "%" . $this->getPlan() . "%";
        $query = $conexion->prepare("SELECT * FROM plan_cuenta 
                                    WHERE plan LIKE :plan AND estado='1'
                                    ORDER BY plan;");
        $query->execute(array('plan' => $likeVar ));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    /**
     * update the data in the table plan_cuenta by id_plan_cuenta
     */ 
    public function updatePlan(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE plan_cuenta
                                    SET codigo = :codigo, nivel = :nivel, plan = :plan
                                    WHERE id_plan_cuenta = :id;");
        $query->execute(array('codigo' => $this->getCodigo(),
            'nivel' => $this->getNivel(),
            'plan' => $this->getPlan(),
            'id' => $this->getIdPlanCuenta()));
        return $query->rowCount();
        $conexion = null;
    }

    /**
     * update the estado in the table plan_cuenta
     */ 
    public function updateEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE plan_cuenta
                                    SET  estado = :estado
                                    WHERE id_plan_cuenta =:id;");
        $query->execute(array(
            'estado' => $this->getEstado(),
            'id' => $this->getIdPlanCuenta()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * insert the data in the table plan_cuenta
     */ 
    public function insertPlanCuenta(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO plan_cuenta(codigo,nivel,plan)
                                    VALUES(:codigo,:nivel,:plan);");
        $query->execute(array('codigo' => $this->getCodigo(),
            'nivel' => $this->getNivel(),
            'plan' => $this->getPlan()));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }


}