<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 03/01/2020
 * Time: 01:12 PM
 */
class Producto{
    
    private $idProducto;
    private $producto;
    private $estado;
    

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of producto
     */ 
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set the value of producto
     *
     * @return  self
     */ 
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get the value of idProducto
     */ 
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set the value of idProducto
     *
     * @return  self
     */ 
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    //--------------------------------------------------------End Getter and Setter---------------------------------------------------------------------------

    /**
     * insert the data in the table producto
     */ 
    public function insertProducto(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO producto(producto,estado)
                                    VALUES (:producto,:estado);");
        $query->execute(array(
            'producto' => $this->getProducto(),
            'estado' => $this->getEstado()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * get the result of the producto table by estado
     */ 
    public function selectProductoByEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_producto, producto, estado
                                    FROM producto
                                    WHERE estado = :estado
                                    ORDER BY id_producto DESC;");
        $query->execute(array('estado' => $this->getEstado()));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    /**
     * get the result of the producto table by estado
     */ 
    public function selectProductByID(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_producto, producto
                                    FROM producto
                                    WHERE id_producto = :id;");
        $query->execute(array('id' => $this->getIdProducto()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    
    /**
     * update the data in the table producto
     */ 
    public function updateProducto(){
        $conexion = new Conexion();
        $query = $conexion->prepare(
            "UPDATE producto
            SET  producto = :producto
            WHERE id_producto =:id;"
        );
        $query->execute(array(
            'producto' => $this->getProducto(),
            'id' => $this->getIdProducto()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * update the estado in the table producto
     */ 
    public function updateEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE producto
                                    SET  estado = :estado
                                     WHERE id_producto =:id;");
        $query->execute(array(
            'estado' => $this->getEstado(),
            'id' => $this->getIdProducto()
        ));
        $conexion = null;
        return $query->rowCount();
    }


    

}
