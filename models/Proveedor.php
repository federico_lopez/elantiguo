<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 28/02/2020
 * Time: 03:28 PM
 */

class Proveedor{

    private $idProveedor;
    private $proveedor;
    private $contacto;
    private $telefono;
    private $email;
    private $celular;
    private $direccion;
    private $ruc;
    private $ciudad;
    private $estado;


    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of ciudad
     */ 
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set the value of ciudad
     *
     * @return  self
     */ 
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get the value of ruc
     */ 
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * Set the value of ruc
     *
     * @return  self
     */ 
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;

        return $this;
    }

    /**
     * Get the value of direccion
     */ 
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set the value of direccion
     *
     * @return  self
     */ 
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get the value of celular
     */ 
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set the value of celular
     *
     * @return  self
     */ 
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of telefono
     */ 
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set the value of telefono
     *
     * @return  self
     */ 
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get the value of contacto
     */ 
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * Set the value of contacto
     *
     * @return  self
     */ 
    public function setContacto($contacto)
    {
        $this->contacto = $contacto;

        return $this;
    }

    /**
     * Get the value of proveedor
     */ 
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set the value of proveedor
     *
     * @return  self
     */ 
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get the value of idProveedor
     */ 
    public function getIdProveedor()
    {
        return $this->idProveedor;
    }

    /**
     * Set the value of idProveedor
     *
     * @return  self
     */ 
    public function setIdProveedor($idProveedor)
    {
        $this->idProveedor = $idProveedor;

        return $this;
    }

//------------------------------------------End Getter and Setter--------------------------------------------------

    public function selectAll(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_proveedor,proveedor,ruc,contacto,telefono,ciudad,celular,direccion,email
                                        FROM proveedor 
                                        WHERE estado='1'
                                        order by id_proveedor desc 
                                        limit 100");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function searchProvider(){
        $conexion = new Conexion();
        $likeVar = "%" . $this->getProveedor() . "%";
        $query = $conexion->prepare("SELECT * FROM proveedor 
                                    WHERE proveedor LIKE :proveedor AND estado='1'
                                    ORDER BY proveedor;");
        $query->execute(array('proveedor' => $likeVar ));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    /**
     * update the estado in the table proveedor
     */ 
    public function updateEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE proveedor
                                    SET  estado = :estado
                                    WHERE id_proveedor =:id;");
        $query->execute(array(
            'estado' => $this->getEstado(),
            'id' => $this->getIdProveedor()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * insert the data in the table proveedor
     */ 
    public function insertProvider(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO proveedor(proveedor,telefono,direccion,contacto,celular,email,ruc,ciudad)
                                    VALUES(:proveedor,:telefono,:direccion,:contacto,:celular,:email,:ruc,:ciudad);");
        $query->execute(array('proveedor' => $this->getProveedor(),
            'telefono' => $this->getTelefono(),
            'direccion' => $this->getDireccion(),
            'contacto' => $this->getContacto(),
            'celular' => $this->getCelular(),
            'email' => $this->getEmail(),
            'ruc' => $this->getRuc(),
            'ciudad'=>$this->getCiudad()));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }

    /**
     * update the data in the table proveedor by id_proveedor
     */ 
    public function updateProvider(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE proveedor
                                    SET proveedor = :proveedor, ruc = :ruc, contacto = :contacto, telefono = :telefono, celular = :celular,
                                    email = :email, ciudad = :localidad, direccion = :direccion
                                    WHERE id_proveedor = :id;");
        $query->execute(array('proveedor' => $this->getProveedor(),
            'ruc' => $this->getRuc(),
            'contacto' => $this->getContacto(),
            'telefono' => $this->getTelefono(),
            'celular' => $this->getCelular(),
            'email' => $this->getEmail(),
            'localidad' => $this->getCiudad(),
            'direccion' => $this->getDireccion(),
            'id' => $this->getIdProveedor()));
        return $query->rowCount();
        $conexion = null;
    }
}