<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 07/02/2020
 * Time: 05:46 PM
 */

class Empleado{

    private $idEmpleado;
    private $nombre;
    private $ci;
    private $telefono;
    private $ciudad;
    private $sueldo;
    private $nacimiento;
    private $cargo;
    private $estado;
    
    /**
     * Get the value of idEmpleado
     */ 
    public function getIdEmpleado()
    {
        return $this->idEmpleado;
    }

    /**
     * Set the value of idEmpleado
     *
     * @return  self
     */ 
    public function setIdEmpleado($idEmpleado)
    {
        $this->idEmpleado = $idEmpleado;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNacimiento()
    {
        return $this->nacimiento;
    }

    /**
     * @param mixed $nacimiento
     */
    public function setNacimiento($nacimiento)
    {
        $this->nacimiento = $nacimiento;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getCi()
    {
        return $this->ci;
    }

    /**
     * @param mixed $ci
     */
    public function setCi($ci)
    {
        $this->ci = $ci;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * @param mixed $ciudad
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return mixed
     */
    public function getSueldo()
    {
        return $this->sueldo;
    }

    /**
     * @param mixed $sueldo
     */
    public function setSueldo($sueldo)
    {
        $this->sueldo = $sueldo;
    }

    /**
     * Get the value of cargo
     */ 
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set the value of cargo
     *
     * @return  self
     */ 
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

     /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    //------------------------------------------End Getter and Setter----------------------------------------------

    public function selectEmpleado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("select * from empleado where estado='1'order by nombre");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function insertEmpleado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO empleado(nombre,telefono,nacimiento,ciudad,cargo,sueldo,ci)
                                    VALUES(:nombre, :telefono, :nacimiento, :ciudad, :cargo, :sueldo, :ci);");
        $query->execute (array('nombre' => $this->getNombre(),
            'telefono' => $this->getTelefono(),
            'nacimiento' => $this->getNacimiento(),
            'ciudad' => $this->getCiudad(),
            'cargo' => $this->getCargo(),
            'sueldo' => $this->getSueldo(),
            'ci' =>$this->getCi()));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }

    public function selectEmpleadoById(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_empleado,nombre,telefono,nacimiento,ciudad,cargo,sueldo,ci 
                                    FROM empleado WHERE id_empleado = :id;");
        $query->execute(array('id' => $this->getIdempleado() ));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function updateEmpleado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE empleado
                                    SET nombre = :nombre, telefono = :telefono, nacimiento = :nacimiento, ciudad = :ciudad,
                                    cargo = :cargo, sueldo = :sueldo, ci = :ci
                                    WHERE id_empleado = :id;");
        $query->execute(array('nombre' => $this->getNombre(),
            'telefono' => $this->getTelefono(),
            'nacimiento' => $this->getNacimiento(),
            'ciudad' => $this->getCiudad(),
            'cargo' => $this->getCargo(),
            'sueldo' => $this->getSueldo(),
            'ci' => $this->getCi(),
            'id' => $this->getIdempleado()));
        return $query->rowCount();
        $conexion = null;
    }


     /**
     * update the estado in the table marca
     */ 
    public function updateEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE empleado
                                    SET  estado = :estado
                                    WHERE id_empleado =:id;");
        $query->execute(array(
            'estado' => $this->getEstado(),
            'id' => $this->getIdEmpleado()
        ));
        $conexion = null;
        return $query->rowCount();
    }
   
}