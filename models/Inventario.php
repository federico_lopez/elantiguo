<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 24/01/2020
 * Time: 01:12 PM
 */

class Inventario{
    private $idInventario;
    private $fecha;
    private $cantidad;
    private $motivo;
    private $pendiente;
    private $movimiento;
    private $idEmpleado;
    private $idStock;

    /**
     * Get the value of idStock
     */ 
    public function getIdStock()
    {
        return $this->idStock;
    }

    /**
     * Set the value of idStock
     *
     * @return  self
     */ 
    public function setIdStock($idStock)
    {
        $this->idStock = $idStock;

        return $this;
    }

    /**
     * Get the value of idEmpleado
     */ 
    public function getIdEmpleado()
    {
        return $this->idEmpleado;
    }

    /**
     * Set the value of idEmpleado
     *
     * @return  self
     */ 
    public function setIdEmpleado($idEmpleado)
    {
        $this->idEmpleado = $idEmpleado;

        return $this;
    }

    /**
     * Get the value of movimiento
     */ 
    public function getMovimiento()
    {
        return $this->movimiento;
    }

    /**
     * Set the value of movimiento
     *
     * @return  self
     */ 
    public function setMovimiento($movimiento)
    {
        $this->movimiento = $movimiento;

        return $this;
    }

    /**
     * Get the value of pendiente
     */ 
    public function getPendiente()
    {
        return $this->pendiente;
    }

    /**
     * Set the value of pendiente
     *
     * @return  self
     */ 
    public function setPendiente($pendiente)
    {
        $this->pendiente = $pendiente;

        return $this;
    }

    /**
     * Get the value of motivo
     */ 
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * Set the value of motivo
     *
     * @return  self
     */ 
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * Get the value of cantidad
     */ 
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set the value of cantidad
     *
     * @return  self
     */ 
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get the value of fecha
     */ 
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set the value of fecha
     *
     * @return  self
     */ 
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get the value of idInventario
     */ 
    public function getIdInventario()
    {
        return $this->idInventario;
    }

    //------------------------------------------End Getter and Setter----------------------------------------------
    /**
     * insert the data in the table inventario
     */ 
    public function insertInventario(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO inventario(fecha,cantidad,motivo,pendiente,movimiento,id_empleado,id_stock)
                                    VALUES (:fecha,:cantidad,:motivo,:pendiente,:movimiento,:id_empleado,:id_stock);");
        $query->execute(array(
            'fecha' => $this->getFecha(),
            'cantidad' => $this->getCantidad(),
            'motivo' => $this->getMotivo(),
            'pendiente' => $this->getPendiente(),
            'movimiento' => $this->getMovimiento(),
            'id_empleado' => $this->getIdEmpleado(),
            'id_stock' => $this->getIdStock()
        ));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }

     /**
     * get the result of the inventario
     */ 
    public function selectInventario(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT i.id_inventario, i.fecha, i.cantidad, i.motivo, i.pendiente, i.movimiento, e.nombre, s.producto
                                    FROM inventario i, empleado e, stock s
                                    WHERE i.id_empleado=e.id_empleado and i.id_stock=s.id_stock
                                    ORDER BY i.id_inventario DESC LIMIT 100;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    /**
     * get the result of the inventario table by movimiento 
     */ 
    public function selectByMovimiento(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT i.id_inventario, i.fecha, i.cantidad, i.motivo, i.pendiente, i.movimiento, e.nombre, s.producto
        FROM inventario i, empleado e, stock s
        WHERE i.id_empleado=e.id_empleado AND i.id_stock=s.id_stock AND i.movimiento=:movimiento
        ORDER BY i.id_inventario DESC LIMIT 100;");
        $query->execute(array('movimiento' => $this->getMovimiento()));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    
}