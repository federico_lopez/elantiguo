<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 01/01/2020
 * Time: 01:12 PM
 */
class Marca{

    private $idMarca;
    private $marca;
    private $estado;
        

    /**
     * Get the value of marca
     */ 
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set the value of marca
     *
     * @return  self
     */ 
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get the value of idMarca
     */ 
    public function getIdMarca()
    {
        return $this->idMarca;
    }

    /**
     * Set the value of idMarca
     *
     * @return  self
     */ 
    public function setIdMarca($idMarca)
    {
        $this->idMarca = $idMarca;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

     //--------------------------------------------------------End Getter and Setter---------------------------------------------------------------------------

    /**
     * insert the data in the table marca
     */ 
    public function insertMarca(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO marca(marca,estado)
                                    VALUES (:marca,:estado);");
        $query->execute(array(
            'marca' => $this->getMarca(),
            'estado' => $this->getEstado()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * get the result of the marca table by estado
     */ 
    public function selectMarcaByEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_marca, marca, estado
                                    FROM marca
                                    WHERE estado = :estado
                                    ORDER BY id_marca DESC;");
        $query->execute(array('estado' => $this->getEstado()));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

     /**
     * get the result of the marca table by id
     */ 
    public function selectMarcaByID(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_marca, marca
                                    FROM marca
                                    WHERE id_marca = :id;");
        $query->execute(array('id' => $this->getIdMarca()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }
    
    /**
     * update the data in the table marca
     */ 
    public function updateMarca(){
        $conexion = new Conexion();
        $query = $conexion->prepare(
            "UPDATE marca
            SET  marca = :marca
            WHERE id_marca =:id;"
        );
        $query->execute(array(
            'marca' => $this->getMarca(),
            'id' => $this->getIdMarca()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * update the estado in the table marca
     */ 
    public function updateEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE marca
                                    SET  estado = :estado
                                     WHERE id_marca =:id;");
        $query->execute(array(
            'estado' => $this->getEstado(),
            'id' => $this->getIdMarca()
        ));
        $conexion = null;
        return $query->rowCount();
    }
}