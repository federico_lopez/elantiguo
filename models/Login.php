<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 13/07/2016
 * Time: 11:08 PM
 */
class Login{

    public $user;
    public $password;
    public $result;

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
  

    public function consult(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_user,user,password,imagen,rol FROM user WHERE user = :usuario AND password = :pass");
        $query->execute(array(':usuario' => $this->getUser(), ':pass' => $this->getPassword()));
        $this->result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
    }


    public function check(){
        if(!empty($this->result)){
            $_SESSION['session'] = $this->result;
            return true;
        }else{
            return false;
        }
    }


    
}