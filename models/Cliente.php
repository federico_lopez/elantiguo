<?php

/**
 * Created by PhpStorm.
 * User: Fede
 * Date: 28/02/2020
 * Time: 3:03 PM
 */

class Cliente{

    private $idCliente;
    private $cliente;
    private $ruc;
    private $contacto;
    private $telefono;
    private $email;
    private $localidad;
    private $celular;
    private $direccion;
    private $estado;

    /**
     * Get the value of contacto
     */ 
    public function getContacto()
    {
        return $this->contacto;
    }

    /**
     * Set the value of contacto
     *
     * @return  self
     */ 
    public function setContacto($contacto)
    {
        $this->contacto = $contacto;

        return $this;
    }
    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of direccion
     */ 
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set the value of direccion
     *
     * @return  self
     */ 
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get the value of celular
     */ 
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set the value of celular
     *
     * @return  self
     */ 
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get the value of localidad
     */ 
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set the value of localidad
     *
     * @return  self
     */ 
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of telefono
     */ 
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set the value of telefono
     *
     * @return  self
     */ 
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get the value of ruc
     */ 
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * Set the value of ruc
     *
     * @return  self
     */ 
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;

        return $this;
    }

    /**
     * Get the value of cliente
     */ 
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set the value of cliente
     *
     * @return  self
     */ 
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get the value of idCliente
     */ 
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set the value of idCliente
     *
     * @return  self
     */ 
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    //------------------------------------------End Getter and Setter----------------------------------------------

    public function selectAll(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_cliente,cliente,ruc,contacto,telefono,email,localidad,celular,direccion 
                                        FROM cliente 
                                        WHERE estado='1'
                                        order by id_cliente desc 
                                        limit 100");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function searchCustomer(){
        $conexion = new Conexion();
        $likeVar = "%" . $this->getCliente() . "%";
        $query = $conexion->prepare("SELECT * FROM cliente 
                                    WHERE cliente LIKE :cliente AND estado='1'
                                    ORDER BY cliente;");
        $query->execute(array('cliente' => $likeVar ));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    /**
     * update the data in the table cliente by id_cliente
     */ 
    public function updateCliente(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE cliente
                                    SET cliente = :cliente, ruc = :ruc, contacto = :contacto, telefono = :telefono, celular = :celular,
                                    email = :email, localidad = :localidad, direccion = :direccion
                                    WHERE id_cliente = :id;");
        $query->execute(array('cliente' => $this->getCliente(),
            'ruc' => $this->getRuc(),
            'contacto' => $this->getContacto(),
            'telefono' => $this->getTelefono(),
            'celular' => $this->getCelular(),
            'email' => $this->getEmail(),
            'localidad' => $this->getLocalidad(),
            'direccion' => $this->getDireccion(),
            'id' => $this->getIdCliente()));
        return $query->rowCount();
        $conexion = null;
    }

    /**
     * update the estado in the table cliente
     */ 
    public function updateEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE cliente
                                    SET  estado = :estado
                                    WHERE id_cliente =:id;");
        $query->execute(array(
            'estado' => $this->getEstado(),
            'id' => $this->getIdCliente()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * insert the data in the table cliente
     */ 
    public function insertCliente(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO cliente(cliente,telefono,direccion,contacto,celular,email,ruc,localidad)
                                    VALUES(:cliente,:telefono,:direccion,:contacto,:celular,:email,:ruc,:ciudad);");
        $query->execute(array('cliente' => $this->getCliente(),
            'telefono' => $this->getTelefono(),
            'direccion' => $this->getDireccion(),
            'contacto' => $this->getContacto(),
            'celular' => $this->getCelular(),
            'email' => $this->getEmail(),
            'ruc' => $this->getRuc(),
            'ciudad'=>$this->getLocalidad()));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }

   
}