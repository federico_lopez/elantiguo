<?php

/**
 * Created by PhpStorm.
 * User: Fede
 * Date: 18/07/2017
 * Time: 06:33 PM
 */

class DetalleActivo{
    
    private $idDetalleActivo;
    private $idActivo;
    private $cantidad;
    private $costo;
    private $detalle;
    private $ivaDiez;
    private $ivaCinco;
    private $exenta;
    private $gravadaDiez;
    private $gravadaCinco;
    private $totales;

    /**
     * Get the value of detalle
     */ 
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set the value of detalle
     *
     * @return  self
     */ 
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdDetalleActivo()
    {
        return $this->idDetalleActivo;
    }

    /**
     * @param mixed $idDetalleActivo
     */
    public function setIdDetalleActivo($idDetalleActivo)
    {
        $this->idDetalleActivo = $idDetalleActivo;
    }

    /**
     * @return mixed
     */
    public function getIdActivo()
    {
        return $this->idActivo;
    }

    /**
     * @param mixed $idActivo
     */
    public function setIdActivo($idActivo)
    {
        $this->idActivo = $idActivo;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

    /**
     * @return mixed
     */
    public function getCosto()
    {
        return $this->costo;
    }

    /**
     * @param mixed $costo
     */
    public function setCosto($costo)
    {
        $this->costo = $costo;
    }

    /**
     * Get the value of totales
     */ 
    public function getTotales()
    {
        return $this->totales;
    }

    /**
     * Set the value of totales
     *
     * @return  self
     */ 
    public function setTotales($totales)
    {
        $this->totales = $totales;

        return $this;
    }

    /**
     * Get the value of gravadaCinco
     */ 
    public function getGravadaCinco()
    {
        return $this->gravadaCinco;
    }

    /**
     * Set the value of gravadaCinco
     *
     * @return  self
     */ 
    public function setGravadaCinco($gravadaCinco)
    {
        $this->gravadaCinco = $gravadaCinco;

        return $this;
    }

    /**
     * Get the value of gravadaDiez
     */ 
    public function getGravadaDiez()
    {
        return $this->gravadaDiez;
    }

    /**
     * Set the value of gravadaDiez
     *
     * @return  self
     */ 
    public function setGravadaDiez($gravadaDiez)
    {
        $this->gravadaDiez = $gravadaDiez;

        return $this;
    }

    /**
     * Get the value of exenta
     */ 
    public function getExenta()
    {
        return $this->exenta;
    }

    /**
     * Set the value of exenta
     *
     * @return  self
     */ 
    public function setExenta($exenta)
    {
        $this->exenta = $exenta;

        return $this;
    }


    /**
     * Get the value of ivaCinco
     */ 
    public function getIvaCinco()
    {
        return $this->ivaCinco;
    }

    /**
     * Set the value of ivaCinco
     *
     * @return  self
     */ 
    public function setIvaCinco($ivaCinco)
    {
        $this->ivaCinco = $ivaCinco;

        return $this;
    }

    /**
     * Get the value of ivaDiez
     */ 
    public function getIvaDiez()
    {
        return $this->ivaDiez;
    }

    /**
     * Set the value of ivaDiez
     * @return  self
     */ 
    public function setIvaDiez($ivaDiez)
    {
        $this->ivaDiez = $ivaDiez;

        return $this;
    }

    //--------------------------------------------------------End Getter and Setter---------------------------------------------------------------------------


    /**
     * @param $id
     * @return array
     */
    public function selectDetalleFactura(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_detalle_activo, idactivo, cantidad, detalle, costo, iva_diez, iva_cinco, exenta, totales
                                    FROM detalle_activo
                                    WHERE idactivo=:id;");
        $query->execute(array('id' => $this->getIdActivo()));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    /**
     * @param $id
     * @return arraySum
     */
    public function selectSumDetalleFactura(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT SUM(iva_diez) iva_diez, SUM(iva_cinco) iva_cinco, SUM(exenta) exenta, SUM(totales) totales
                                    FROM detalle_activo
                                    WHERE idactivo=:id;");
        $query->execute(array('id' => $this->getIdActivo()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    /**
     * Insert data in table detalle_activo
     */
    public function insertDetalleActivo(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO detalle_activo(detalle,cantidad,costo,idactivo,iva_diez,iva_cinco,exenta,gravada_diez,gravada_cinco,totales)
                                    VALUES(:detalle,:cantidad,:costo,:idactivo,:iva_diez,:iva_cinco,:exenta,:gravada_diez,:gravada_cinco,:totales);");
        $query->execute(array('detalle' => $this->getDetalle(),
            'cantidad' => $this->getCantidad(),
            'costo' => $this->getCosto(),
            'idactivo' => $this->getIdActivo(),
            'iva_diez' => $this->getIvaDiez(),
            'iva_cinco' => $this->getIvaCinco(),
            'exenta' => $this->getExenta(),
            'gravada_diez' => $this->getGravadaDiez(),
            'gravada_cinco' => $this->getGravadaCinco(),
            'totales' => $this->getTotales()
        ));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }

    /**
     * @param $id
     * Delete data in table detalle_activo
     */
    public function delDetalleActivo(){
        $conexion = new Conexion();
        $query = $conexion->prepare("DELETE FROM detalle_activo WHERE id_detalle_activo=:id;");
        $query->execute(array('id' => $this->getIdDetalleActivo()));
        $conexion = null;
        return $query->rowCount();
    }
   
    
}