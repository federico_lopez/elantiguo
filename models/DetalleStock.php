<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 01/01/2020
 * Time: 01:12 PM
 */
class DetalleStock{
    private $idDetalleStock;
    private $detalleStock;
    private $estado;
    private $idProducto;
    private $idCaracteristica;
    private $idMedida;
    private $idDeposito;
    private $caracteristica;

    /**
     * Get the value of idDeposito
     */ 
    public function getIdDeposito()
    {
        return $this->idDeposito;
    }

    /**
     * Set the value of idDeposito
     *
     * @return  self
     */ 
    public function setIdDeposito($idDeposito)
    {
        $this->idDeposito = $idDeposito;

        return $this;
    }

    /**
     * Get the value of detalleStock
     */ 
    public function getDetalleStock()
    {
        return $this->detalleStock;
    }

    /**
     * Set the value of detalleStock
     *
     * @return  self
     */ 
    public function setDetalleStock($detalleStock)
    {
        $this->detalleStock = $detalleStock;

        return $this;
    }

    /**
     * Get the value of idDetalleStock
     */ 
    public function getIdDetalleStock()
    {
        return $this->idDetalleStock;
    }

    /**
     * Set the value of idDetalleStock
     *
     * @return  self
     */ 
    public function setIdDetalleStock($idDetalleStock)
    {
        $this->idDetalleStock = $idDetalleStock;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of idProducto
     */ 
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set the value of idProducto
     *
     * @return  self
     */ 
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get the value of idMedida
     */ 
    public function getIdMedida()
    {
        return $this->idMedida;
    }

    /**
     * Set the value of idMedida
     *
     * @return  self
     */ 
    public function setIdMedida($idMedida)
    {
        $this->idMedida = $idMedida;

        return $this;
    }



    

    /**
     * Get the value of idCaracteristica
     */ 
    public function getIdCaracteristica()
    {
        return $this->idCaracteristica;
    }

    /**
     * Set the value of idCaracteristica
     *
     * @return  self
     */ 
    public function setIdCaracteristica($idCaracteristica)
    {
        $this->idCaracteristica = $idCaracteristica;

        return $this;
    }

//---------------------------------------------End Getter and Setter-----------------------------------------------

    /**
     * insert the data in the table detalle_stock
     */ 
    public function insertDetalleStock(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO detalle_stock(detalle_stock,id_producto,id_caracteristica,id_medida,id_deposito)
                                    VALUES (:detalle_stock,:id_producto,:id_caracteristica,:id_medida,:id_deposito);");
        $query->execute(array(
            'detalle_stock' => $this->getDetalleStock(),
            'id_producto' => $this->getIdProducto(),
            'id_caracteristica' => $this->getIdCaracteristica(),
            'id_medida' => $this->getIdMedida(),
            'id_deposito' => $this->getIdDeposito()
        ));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }


}