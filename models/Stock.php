<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 01/01/2020
 * Time: 01:12 PM
 */
class Stock
{
    private $idStock;
    private $producto;
    private $stockMin;
    private $cantidad;
    private $costo;
    private $ubicacion;
    private $codigo;
    private $idDetalleStock;
    

    /**
     * Get the value of idDetalleStock
     */ 
    public function getIdDetalleStock()
    {
        return $this->idDetalleStock;
    }

    /**
     * Set the value of idDetalleStock
     *
     * @return  self
     */ 
    public function setIdDetalleStock($idDetalleStock)
    {
        $this->idDetalleStock = $idDetalleStock;

        return $this;
    }

    

    /**
     * Get the value of codigo
     */ 
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set the value of codigo
     *
     * @return  self
     */ 
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get the value of ubicacion
     */ 
    public function getUbicacion()
    {
        return $this->ubicacion;
    }

    /**
     * Set the value of ubicacion
     *
     * @return  self
     */ 
    public function setUbicacion($ubicacion)
    {
        $this->ubicacion = $ubicacion;

        return $this;
    }

    /**
     * Get the value of costo
     */ 
    public function getCosto()
    {
        return $this->costo;
    }

    /**
     * Set the value of costo
     *
     * @return  self
     */ 
    public function setCosto($costo)
    {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get the value of cantidad
     */ 
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set the value of cantidad
     *
     * @return  self
     */ 
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get the value of stockMin
     */ 
    public function getStockMin()
    {
        return $this->stockMin;
    }

    /**
     * Set the value of stockMin
     *
     * @return  self
     */ 
    public function setStockMin($stockMin)
    {
        $this->stockMin = $stockMin;

        return $this;
    }

    /**
     * Get the value of producto
     */ 
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set the value of producto
     *
     * @return  self
     */ 
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get the value of idStock
     */ 
    public function getIdStock()
    {
        return $this->idStock;
    }

    /**
     * Set the value of idStock
     *
     * @return  self
     */ 
    public function setIdStock($idStock)
    {
        $this->idStock = $idStock;

        return $this;
    }

    //---------------------------------------------End Getter and Setter-----------------------------------------------
    /**
     * insert the data in the table stock
     */ 
    public function insertStock(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO stock(producto,stock_min,cantidad,costo,ubicacion,codigo,id_detalle_stock)
                                    VALUES (:producto,:stock_min,:cantidad,:costo,:ubicacion,:codigo,:id_detalle_stock);");
        $query->execute(array(
            'producto' => $this->getProducto(),
            'stock_min' => $this->getStockMin(),
            'cantidad' => $this->getCantidad(),
            'costo' => $this->getCosto(),
            'ubicacion' => $this->getUbicacion(),
            'codigo' => $this->getCodigo(),
            'id_detalle_stock' => $this->getIdDetalleStock()
        ));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }

    /**
     * get the result of the stock
     */ 
    public function selectStock(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_stock,producto,stock_min,cantidad,costo,ubicacion,codigo,id_detalle_stock
                                    FROM stock
                                    ORDER BY id_stock DESC;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    /**
     * get the result of the stock by code fetch
     */ 
    public function selectStockByCode(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_stock,producto,stock_min,cantidad,costo,ubicacion,codigo,id_detalle_stock
                                    FROM stock
                                    WHERE codigo=:code");
        $query->execute(array('code' => $this->getCodigo()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    /**
     * update cantidad in the table stock
     */ 
    public function updateQuantity(){
        $conexion = new Conexion();
        $query = $conexion->prepare(
            "UPDATE stock
            SET  cantidad = :cantidad
            WHERE id_stock =:id;"
        );
        $query->execute(array(
            'cantidad' => $this->getCantidad(),
            'id' => $this->getIdStock()
        ));
        $conexion = null;
        return $query->rowCount();
    }
    
}