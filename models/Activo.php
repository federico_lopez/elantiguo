<?php

/**
 * Created by PhpStorm.
 * User: Fede
 * Date: 10/07/2017
 * Time: 06:43 PM
 */
class Activo{

    private $idActivo;
    private $fecha;
    private $nroFactura;
    private $idCliente;
    private $estado;
    private $remision;
    private $condicion; //contado/credito
    private $idDocumento; 
    private $idTimbrado;
    private $idPlanCuenta;


    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }


    /**
     * @return mixed
     */
    public function getIdActivo()
    {
        return $this->idActivo;
    }

    /**
     * @param mixed $idActivo
     */
    public function setIdActivo($idActivo)
    {
        $this->idActivo = $idActivo;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getNroFactura()
    {
        return $this->nroFactura;
    }

    /**
     * @param mixed $nroFactura
     */
    public function setNroFactura($nroFactura)
    {
        $this->nroFactura = $nroFactura;
    }

    /**
     * @return mixed
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * @param mixed $idCliente
     */
    public function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }
  
    /**
     * Get the value of idTimbrado
     */ 
    public function getIdTimbrado()
    {
        return $this->idTimbrado;
    }

    /**
     * Set the value of idTimbrado
     *
     * @return  self
     */ 
    public function setIdTimbrado($idTimbrado)
    {
        $this->idTimbrado = $idTimbrado;

        return $this;
    }


    /**
     * Get the value of idDocumento
     */ 
    public function getIdDocumento()
    {
        return $this->idDocumento;
    }

    /**
     * Set the value of idDocumento
     *
     * @return  self
     */ 
    public function setIdDocumento($idDocumento)
    {
        $this->idDocumento = $idDocumento;

        return $this;
    }

    /**
     * Get the value of condicion
     */ 
    public function getCondicion()
    {
        return $this->condicion;
    }

    /**
     * Set the value of condicion
     *
     * @return  self
     */ 
    public function setCondicion($condicion)
    {
        $this->condicion = $condicion;

        return $this;
    }

    /**
     * Get the value of remision
     */ 
    public function getRemision()
    {
        return $this->remision;
    }

    /**
     * Set the value of remision
     *
     * @return  self
     */ 
    public function setRemision($remision)
    {
        $this->remision = $remision;

        return $this;
    }

    /**
     * Get the value of idPlanCuenta
     */ 
    public function getIdPlanCuenta()
    {
        return $this->idPlanCuenta;
    }

    /**
     * Set the value of idPlanCuenta
     *
     * @return  self
     */ 
    public function setIdPlanCuenta($idPlanCuenta)
    {
        $this->idPlanCuenta = $idPlanCuenta;

        return $this;
    }
    
    //--------------------------------------------------------End Getter and Setter---------------------------------------------------------------------------


    /**
     * @param $mes
     * @param $anho
     * @return array
     */
    public function ingressMonth($mes, $anho){

        $sentencia = "SELECT a.idactivo, a.fecha, a.nro_factura, cl.cliente, cl.ruc, t.numero, c.condicion, p.plan, a.id_documento, a.remision, a.estado
        FROM activo a, condicion_factura c, cliente cl, plan_cuenta p, timbrado t
        WHERE a.id_cliente = cl.id_cliente AND a.condicion=c.idcondicion_factura AND a.id_plan_cuenta = p.id_plan_cuenta AND a.id_timbrado=t.id_timbrado 
        AND MONTH(a.fecha)=$mes and YEAR(a.fecha)=$anho
        ORDER BY a.nro_factura DESC;";

        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    /**
     * @param $id
     * @return array
     */
    public function selectActivoById(){

        $sentencia = "SELECT a.idactivo, a.fecha, a.nro_factura, a.id_cliente, a.id_timbrado, a.condicion, a.id_plan_cuenta, a.id_documento, a.remision, a.estado
        FROM activo a
        WHERE a.idactivo=:id
        GROUP BY a.idactivo;";

        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute(array('id' => $this->getIdActivo()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;

    }

    /**
     * Inserta los datos en el activo
     */
    public function insertActivo(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO activo(fecha, nro_factura, estado, condicion, id_cliente, id_plan_cuenta, id_timbrado, id_documento, remision)
                                    VALUES (:fecha,:nro_factura,:estado,:condicion,:id_cliente,:id_plan_cuenta,:id_timbrado,:id_documento,:remision);");
        $query->execute(array('fecha' => $this->getFecha(),
            'nro_factura' => $this->getNroFactura(),
            'estado' => $this->getEstado(),
            'condicion' => $this->getCondicion(),
            'id_cliente' => $this->getIdCliente(),
            'id_plan_cuenta' => $this->getIdPlanCuenta(),
            'id_timbrado' => $this->getIdTimbrado(),
            'id_documento' => $this->getIdDocumento(),
            'remision' => $this->getRemision()));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }

    /**
     * 
     */
    public function reportActivo(){

        $sentencia = "SELECT a.idactivo, a.fecha, a.nro_factura, cl.cliente, cl.ruc, t.numero, c.condicion, p.plan, a.id_documento, a.remision,
        SUM(d.monto_diez) monto_diez, SUM(d.monto_cinco) monto_cinco, SUM(d.iva_diez) iva_diez, SUM(d.iva_cinco) iva_cinco, SUM(d.exenta) exenta, SUM(d.totales) totales
        FROM activo a, condicion_factura c, cliente cl, plan_cuenta p, timbrado t, detalle_activo d
        WHERE a.id_cliente = cl.id_cliente AND a.condicion=c.idcondicion_factura AND a.id_plan_cuenta = p,id_plan_cuenta AND a.id_timbrado=t.id_timbrado AND a.idactivo=d.idactivo
        AND a.estado='1'
        GROUP BY a.idactivo
        ORDER BY a.nro_factura;";

        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }
  
    
}