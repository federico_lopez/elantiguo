<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 01/01/2020
 * Time: 01:12 PM
 */
class Caracteristica{

    private $idCaracteristica;
    private $caracteristica;
    private $estado;
        
   /**
     * Get the value of caracteristica
     */ 
    public function getCaracteristica()
    {
        return $this->caracteristica;
    }

    /**
     * Set the value of caracteristica
     *
     * @return  self
     */ 
    public function setCaracteristica($caracteristica)
    {
        $this->caracteristica = $caracteristica;

        return $this;
    }

    /**
     * Get the value of idCaracteristica
     */ 
    public function getIdCaracteristica()
    {
        return $this->idCaracteristica;
    }

    /**
     * Set the value of idCaracteristica
     *
     * @return  self
     */ 
    public function setIdCaracteristica($idCaracteristica)
    {
        $this->idCaracteristica = $idCaracteristica;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

     //--------------------------------------------------------End Getter and Setter---------------------------------------------------------------------------

    /**
     * insert the data in the table caracteristica
     */ 
    public function insertCaracteristica(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO caracteristica(caracteristica,estado)
                                    VALUES (:caracteristica,:estado);");
        $query->execute(array(
            'caracteristica' => $this->getCaracteristica(),
            'estado' => $this->getEstado()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * get the result of the caracteristica table by estado
     */ 
    public function selectCaracteristicaByEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_caracteristica, caracteristica, estado
                                    FROM caracteristica
                                    WHERE estado = :estado
                                    ORDER BY id_caracteristica DESC;");
        $query->execute(array('estado' => $this->getEstado()));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

     /**
     * get the result of the caracteristica table by id
     */ 
    public function selectCaracteristicaByID(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT id_caracteristica, caracteristica
                                    FROM caracteristica
                                    WHERE id_caracteristica = :id;");
        $query->execute(array('id' => $this->getIdCaracteristica()));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }
    
    /**
     * update the data in the table caracteristica
     */ 
    public function updateCaracteristica(){
        $conexion = new Conexion();
        $query = $conexion->prepare(
            "UPDATE caracteristica
            SET  caracteristica = :caracteristica
            WHERE id_caracteristica =:id;"
        );
        $query->execute(array(
            'caracteristica' => $this->getCaracteristica(),
            'id' => $this->getIdCaracteristica()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    /**
     * update the estado in the table caracteristica
     */ 
    public function updateEstado(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE caracteristica
                                    SET  estado = :estado
                                     WHERE id_caracteristica =:id;");
        $query->execute(array(
            'estado' => $this->getEstado(),
            'id' => $this->getIdCaracteristica()
        ));
        $conexion = null;
        return $query->rowCount();
    }

    
}